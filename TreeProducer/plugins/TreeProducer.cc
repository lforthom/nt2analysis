// -*- C++ -*-
//
// Package:    nt2analysis/TreeProducer
// Class:      TreeProducer
//
//
// Original Author:  Laurent Forthomme
//         Created:  Fri, 07 Jul 2023 13:33:11 GMT
//
//

#include <memory>

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "FWCore/Utilities/interface/InputTag.h"

#include "DataFormats/CTPPSDetId/interface/TotemT2DetId.h"
#include "DataFormats/Common/interface/DetSetVectorNew.h"
#include "DataFormats/TotemReco/interface/TotemT2Digi.h"

#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "TTree.h"

class TreeProducer : public edm::one::EDAnalyzer<edm::one::SharedResources> {
public:
  explicit TreeProducer(const edm::ParameterSet& iConfig)
      : digisToken_(consumes<edmNew::DetSetVector<TotemT2Digi> >(iConfig.getParameter<edm::InputTag>("digisTag"))) {
    usesResource("TFileService");
    edm::Service<TFileService> fs;
    tree_ = fs->make<TTree>("tree", "Events kinematics tree");
  }
  ~TreeProducer() override = default;

  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
    edm::ParameterSetDescription desc;
    desc.add<edm::InputTag>("digisTag", edm::InputTag("totemT2Digis", "TotemT2"))
        ->setComment("input digis collection to retrieve");
    descriptions.add("treeProducer", desc);
  }

private:
  // ------------ method called once each job just before starting event loop  ------------
  void beginJob() override {
    if (!tree_)
      throw cms::Exception("TreeProducer") << "The tree has not been initialised by the TFileService!";
    tree_->Branch("digis_arm", &digis_arm_);
    tree_->Branch("digis_plane", &digis_plane_);
    tree_->Branch("digis_channel", &digis_channel_);
    tree_->Branch("digis_leadingedges", &digis_leadingedges_);
    tree_->Branch("digis_trailingedges", &digis_trailingedges_);
    tree_->Branch("digis_statuses", &digis_statuses_);
  }

  // ------------ method called for each event  ------------
  void analyze(const edm::Event& iEvent, const edm::EventSetup&) override {
    digis_arm_.clear();
    digis_plane_.clear();
    digis_channel_.clear();
    digis_leadingedges_.clear();
    digis_trailingedges_.clear();
    digis_statuses_.clear();
    for (const auto& ds_digis : iEvent.get(digisToken_)) {
      const TotemT2DetId detid(ds_digis.detId());  // defines the location of the tile
      for (const auto& digi : ds_digis) {
        digis_arm_.emplace_back(detid.arm());  //FIXME might be overkill to save this instead of the raw detid...
        digis_plane_.emplace_back(detid.plane());
        digis_channel_.emplace_back(detid.channel());
        digis_leadingedges_.emplace_back(digi.leadingEdge());
        digis_trailingedges_.emplace_back(digi.trailingEdge());
        digis_statuses_.emplace_back(digi.status());
      }
    }
    // fill at the end of each event
    tree_->Fill();
  }

  // readout tokens
  edm::EDGetTokenT<edmNew::DetSetVector<TotemT2Digi> > digisToken_;

  // tree-based info
  TTree* tree_{nullptr};
  std::vector<unsigned int> digis_arm_, digis_plane_, digis_channel_, digis_leadingedges_, digis_trailingedges_,
      digis_statuses_;
};

//define this as a plug-in
DEFINE_FWK_MODULE(TreeProducer);
