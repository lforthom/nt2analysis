import FWCore.ParameterSet.Config as cms
process = cms.Process('T2')

process.load('FWCore.MessageService.MessageLogger_cfi')
process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(5000)
)

process.source = cms.Source('PoolSource',
    fileNames = cms.untracked.vstring(
        'PUT_HERE_YOUR_EDM_FILE_PATH',  # drop the /eos/cms, e.g. start from /store/data/...
    )
)

# prepare the output file
process.TFileService = cms.Service("TFileService",
    fileName = cms.string("histo.root"),
    closeFileFast = cms.untracked.bool(True)
)

process.load('nt2Analysis.TreeProducer.treeProducer_cfi')

process.p = cms.Path(
    process.treeProducer
)
